import {findServer, Server} from 'assess-server-module/src';

const servers: Server[] = [
    {
        "url": "https://does-not-work.perfume.new",
        "priority": 1
    },
    {
        "url": "https://gitlab.com",
        "priority": 4
    },
    {
        "url": "https://app.scnt.me",
        "priority": 3
    },
    {
        "url": "https://offline.scentronix.com",
        "priority": 2
    }
];

findServer(servers).then((r: any) => {
    console.log('Here is a list of online servers:', r)
}).catch((err: any) => {
    console.error(err)
})