# Server assess service

This repository consists of 2 folders:
- `assess-server-module`: a module that function as a server assess service
- `demo-application`: index.js file demonstrate how the above module can be used

## Setup

```console
cd assess-server-module
npm install
npm run build
cd ../demo-application
npm install
npm run build
npm start
```

## Test
```console
cd assess-server-module
npm test
```

### Test areas
- Test assess servers and return results
- Request checking server's availability should timeout after 5 seconds