import {findServer} from "../src";
import fetchMock from "jest-fetch-mock"

beforeEach(() => {
    fetchMock.resetMocks();
});

describe('testing timeouts', () => {
    it('request should timeout', async () => {
        const timeout = 2000
        fetchMock.mockResponseOnce(
            () =>
                new Promise(resolve => setTimeout(() => resolve({body: 'timeout', status: 504}), timeout+100))
        )
        await expect(findServer([
            {
                "url": "https://does-not-work.perfume.new",
                "priority": 1
            },
            {
                "url": "https://gitlab.com",
                "priority": 4
            },
            {
                "url": "https://app.scnt.me",
                "priority": 3
            },
            {
                "url": "https://offline.scentronix.com",
                "priority": 2
            }
        ], timeout))
            .rejects
            .toThrow('Request timeout');
    })
})
