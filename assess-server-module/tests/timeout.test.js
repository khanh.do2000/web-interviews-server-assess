"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const src_1 = require("../src");
const jest_fetch_mock_1 = __importDefault(require("jest-fetch-mock"));
beforeEach(() => {
    jest_fetch_mock_1.default.resetMocks();
});
describe('testing timeouts', () => {
    it('request should timeout', async () => {
        const timeout = 2000;
        jest_fetch_mock_1.default.mockResponseOnce(() => new Promise(resolve => setTimeout(() => resolve({ body: 'timeout', status: 504 }), timeout + 100)));
        await expect((0, src_1.findServer)([
            {
                "url": "https://does-not-work.perfume.new",
                "priority": 1
            },
            {
                "url": "https://gitlab.com",
                "priority": 4
            },
            {
                "url": "https://app.scnt.me",
                "priority": 3
            },
            {
                "url": "https://offline.scentronix.com",
                "priority": 2
            }
        ], timeout))
            .rejects
            .toThrow('Request timeout');
    });
});
