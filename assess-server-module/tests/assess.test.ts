import {findServer} from "../src";
import fetchMock from "jest-fetch-mock"

beforeEach(() => {
    fetchMock.resetMocks();
});

describe('testing assess api', () => {
    beforeEach(() => {
        fetchMock.resetMocks()

        fetchMock.mockIf(/^https?:\/\/does-not-work.perfume.new.*$/, async (req) => {
            return {
                status: 404,
                body: 'Not Found'
            }
        })
        fetchMock.mockIf(/^https?:\/\/gitlab.com.*$/, async (req) => {
            return {
                status: 200,
                body: 'ok'
            }
        })
        fetchMock.mockIf(/^https?:\/\/app.scnt.me.*$/, async (req) => {
            return {
                status: 200,
                body: 'ok'
            }
        })
        fetchMock.mockIf(/^https?:\/\/offline.scentronix.com.*$/, async (req) => {
            return {
                status: 404,
                body: 'Not Found'
            }
        })
    })

    it('assess servers and return results', () => {
        findServer([
            {
                "url": "https://does-not-work.perfume.new",
                "priority": 1
            },
            {
                "url": "https://gitlab.com",
                "priority": 4
            },
            {
                "url": "https://app.scnt.me",
                "priority": 3
            },
            {
                "url": "https://offline.scentronix.com",
                "priority": 2
            }
        ]).then((res: any) => {
            expect(res).toEqual([
                {url: 'https://app.scnt.me', priority: 3, status: 200},
                {url: 'https://gitlab.com', priority: 4, status: 200}
            ])
        })
    })
})
