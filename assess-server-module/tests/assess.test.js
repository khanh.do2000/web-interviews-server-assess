"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const src_1 = require("../src");
const jest_fetch_mock_1 = __importDefault(require("jest-fetch-mock"));
beforeEach(() => {
    jest_fetch_mock_1.default.resetMocks();
});
describe('testing assess api', () => {
    beforeEach(() => {
        jest_fetch_mock_1.default.resetMocks();
        jest_fetch_mock_1.default.mockIf(/^https?:\/\/does-not-work.perfume.new.*$/, async (req) => {
            return {
                status: 404,
                body: 'Not Found'
            };
        });
        jest_fetch_mock_1.default.mockIf(/^https?:\/\/gitlab.com.*$/, async (req) => {
            return {
                status: 200,
                body: 'ok'
            };
        });
        jest_fetch_mock_1.default.mockIf(/^https?:\/\/app.scnt.me.*$/, async (req) => {
            return {
                status: 200,
                body: 'ok'
            };
        });
        jest_fetch_mock_1.default.mockIf(/^https?:\/\/offline.scentronix.com.*$/, async (req) => {
            return {
                status: 404,
                body: 'Not Found'
            };
        });
    });
    it('assess servers and return results', () => {
        (0, src_1.findServer)([
            {
                "url": "https://does-not-work.perfume.new",
                "priority": 1
            },
            {
                "url": "https://gitlab.com",
                "priority": 4
            },
            {
                "url": "https://app.scnt.me",
                "priority": 3
            },
            {
                "url": "https://offline.scentronix.com",
                "priority": 2
            }
        ]).then((res) => {
            expect(res).toEqual([
                { url: 'https://app.scnt.me', priority: 3, status: 200 },
                { url: 'https://gitlab.com', priority: 4, status: 200 }
            ]);
        });
    });
});
