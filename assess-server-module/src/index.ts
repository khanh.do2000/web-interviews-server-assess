import fetch from 'node-fetch';
import {isAValidUrl, wait} from "./utils";

/** @type {Number} */
const defaultTimeout = 5000;

export interface Server {
    url: string,
    priority: number
}

export interface ServerResult extends Server {
    status: number
}

/** @type {ServerResult} */
function filterByStatus(result: ServerResult) {
    return result.status >= 200 && result.status <= 299;
}

/**
 * @param {ServerResult}  results - List of server to be assessed.
 * @returns {ServerResult} List of online servers
 */
function filterAndSortServerResult(results: Awaited<ServerResult>[]) {
    return results
        .filter(result => filterByStatus(result))
        .sort((a, b) => a.priority - b.priority);
}

/**
 * @param {string}  servers - List of server to be assessed.
 * @param {string=} timeout - Default value of timeout if 5000ms
 * @returns {ServerResult} List of online servers
 */
export async function findServer(
    servers: Server[],
    timeout: number = defaultTimeout
): Promise<ServerResult[]> {
    return new Promise<ServerResult[]>((resolve, reject) => {
        Promise.race([
            Promise.all([
                ...servers.map((server: Server) => (
                    new Promise<ServerResult>((resolve, reject) => {
                        if (!isAValidUrl(server.url)) {
                            throw new Error('Invalid URL')
                        }
                        fetch(server.url)
                            .then((r: any) =>
                                resolve({
                                    ...server,
                                    status: r.status,
                                }))
                            .catch(err =>
                                resolve({
                                    ...server,
                                    status: 0
                                })
                            )
                    })
                ))
            ]).then((results) => {
                const onlineServers = filterAndSortServerResult(results)
                if (onlineServers.length) {
                    resolve(onlineServers)
                } else {
                    reject(new Error('no server online'))
                }
            }),
            wait(timeout).then(() => {
                reject(new Error("Request timeout after " + timeout + " ms"))
            })
        ])
    });
}
