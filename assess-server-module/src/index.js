"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.findServer = void 0;
const node_fetch_1 = __importDefault(require("node-fetch"));
const utils_1 = require("./utils");
const defaultTimeout = 5000;
function filterByStatus(result) {
    return result.status >= 200 && result.status <= 299;
}
function filterAndSortServerResult(results) {
    return results
        .filter(result => filterByStatus(result))
        .sort((a, b) => a.priority - b.priority);
}
async function findServer(servers, timeout = defaultTimeout) {
    return new Promise((resolve, reject) => {
        Promise.race([
            Promise.all([
                ...servers.map((server) => (new Promise((resolve, reject) => {
                    if (!(0, utils_1.isAValidUrl)(server.url)) {
                        throw new Error('Invalid URL');
                    }
                    (0, node_fetch_1.default)(server.url)
                        .then((r) => resolve({
                        ...server,
                        status: r.status,
                    }))
                        .catch(err => resolve({
                        ...server,
                        status: 0
                    }));
                })))
            ]).then((results) => {
                const onlineServers = filterAndSortServerResult(results);
                if (onlineServers.length) {
                    resolve(onlineServers);
                }
                else {
                    reject(new Error('no server online'));
                }
            }),
            (0, utils_1.wait)(timeout).then(() => {
                reject(new Error("Request timeout after " + timeout + " ms"));
            })
        ]);
    });
}
exports.findServer = findServer;
