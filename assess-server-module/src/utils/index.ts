import {URL} from "url";

/**
 * @param {number}  ms - Timeout value in milliseconds.
 * @returns {Promise} List of online servers
 */
export const wait = (ms: number): Promise<any> => new Promise(resolve => setTimeout(resolve, ms));
/**
 * @param {string}  url - List of server to be assessed.
 * @returns {boolean}
 */
export const isAValidUrl = (url: string) => {
    try {
        new URL(url);
        return true;
    } catch (err) {
        return false;
    }
};