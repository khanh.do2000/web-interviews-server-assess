"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isAValidUrl = exports.wait = void 0;
const url_1 = require("url");
const wait = (ms) => new Promise(resolve => setTimeout(resolve, ms));
exports.wait = wait;
const isAValidUrl = (s) => {
    try {
        new url_1.URL(s);
        return true;
    }
    catch (err) {
        return false;
    }
};
exports.isAValidUrl = isAValidUrl;
